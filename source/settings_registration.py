AUTHENTICATION_BACKENDS = (
  'social.backends.facebook.FacebookOAuth2',
  'social.backends.twitter.TwitterOAuth',
  'social.backends.vk.VKOAuth2',
  'social.backends.google.GoogleOAuth2',
  'django.contrib.auth.backends.ModelBackend',
)

# SOCIAL_AUTH_RAISE_EXCEPTIONS = False
SOCIAL_AUTH_LOGIN_REDIRECT_URL = '/dashboard/'
SOCIAL_AUTH_LOGIN_ERROR_URL = '/dashboard/login-error/'



SOCIAL_AUTH_PIPELINE = (
    'social.pipeline.social_auth.social_details',
    'social.pipeline.social_auth.social_uid',
    'social.pipeline.social_auth.auth_allowed',
    'social.pipeline.social_auth.social_user',
    'social.pipeline.user.get_username',
    'social.pipeline.mail.mail_validation',
    # 'social.pipeline.social_auth.associate_by_email',
    'social.pipeline.user.create_user',
    'social.pipeline.social_auth.associate_user',
    'social.pipeline.social_auth.load_extra_data',
    'social.pipeline.user.user_details',
    'apps.dashboard.pipeline.get_profile_picture',
)

# App facebook for site
SOCIAL_AUTH_FACEBOOK_KEY = '346173235728093'
SOCIAL_AUTH_FACEBOOK_SECRET = 'f1ed37a0d453c477f5fb054a2d02bc24'
SOCIAL_AUTH_FACEBOOK_SCOPE = ['email']
SOCIAL_AUTH_FACEBOOK_PROFILE_EXTRA_PARAMS = {
  'locale': 'ru_RU',
  'fields': 'email,name,first_name,last_name'
}


SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = '929274030136-jqpqtb6pj8t5ts2bhaptmnlick02rskd.apps.googleusercontent.com'
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = '2hh1_Ep6lBqjLZLIne0NqPSL'
# SOCIAL_AUTH_GOOGLE_OAUTH2_IGNORE_DEFAULT_SCOPE = True
SOCIAL_AUTH_GOOGLE_OAUTH2_SCOPE = [
'https://www.googleapis.com/auth/userinfo.email',
'https://www.googleapis.com/auth/userinfo.profile'
]

SOCIAL_AUTH_VK_OAUTH2_KEY = '5614769'
SOCIAL_AUTH_VK_OAUTH2_SECRET = '4YpCIItL8chDwAgcfvfp'
SOCIAL_AUTH_VK_OAUTH2_SCOPE = ['email']

SOCIAL_AUTH_TWITTER_KEY = 'Pj5fg5uFEMCdyD8ciaykjNY6R'
SOCIAL_AUTH_TWITTER_SECRET = 'aS8MAAoRs0dIJ4sWM6DL6vHF3AlUMq42y8RjhHssRu6nY9NShz'

