# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0003_auto_20170228_2159'),
    ]

    operations = [
        migrations.AlterField(
            model_name='importtask',
            name='status',
            field=models.IntegerField(default=0, verbose_name='\u0421\u0442\u0430\u0442\u0443\u0441 \u0437\u0430\u0434\u0430\u043d\u0438\u044f', choices=[(0, '\u041f\u0440\u0438\u043d\u044f\u0442'), (1, '\u041e\u0431\u0440\u0430\u0431\u0430\u0442\u044b\u0432\u0430\u0435\u0442\u0441\u044f'), (2, '\u0412\u044b\u043f\u043e\u043b\u043d\u0435\u043d'), (3, '\u041e\u0448\u0438\u0431\u043a\u0430 \u0432\u044b\u043f\u043e\u043b\u043d\u0435\u043d\u0438\u044f'), (4, '\u041f\u0440\u0438\u043e\u0441\u0442\u0430\u043d\u043e\u0432\u043b\u0435\u043d'), (5, '\u041f\u0440\u043e\u0441\u0440\u043e\u0447\u0435\u043d')]),
        ),
    ]
