# -*- coding: utf-8 -*-
'''
Created on 28.03.2014

@author: Extertioner
'''

# dictionary for string translate
CHAR_MAP = {'en': 'qwertyuiop[]asdfghjkl;\'zxcvbnm,/QWERTYUIOP{}ASDFGHJKL:"ZXCVBNM<>?.',
            'ru': 'йцукенгшщзхъфывапролджэячсмитьбюЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ.ю'\
                    .decode('utf-8').encode('cp1251')
}
