# -*- coding: utf-8 -*-

import django, os, glob

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'source.settings')
django.setup()

from django.shortcuts import get_object_or_404
from django.conf import settings
from apps.catalog.models import Item, Catalog, Category, Vendor
from apps.website.models import Website, Point
from django.contrib.auth.models import User
from django.db import IntegrityError

import xml.etree.ElementTree as ET
from xml.dom import minidom
from datetime import datetime, timedelta

import random



# get list of files in folder
# use glob to find all .yml files in folder
def get_list_of_files(base_folder):
    # return os.listdir(base_folder)
    return glob.glob("{}*.yml".format(base_folder))


# во время импорта, проверять время в файле, если больше недели, не импортировать
def add_to_db_or_not(xml_date):
    xml_date_convert = datetime.strptime(xml_date, '%Y-%m-%d %H:%M')
    xml_seven_days = xml_date_convert + timedelta(days=7)
    now = datetime.now()
    return 'create' if xml_seven_days > now else 'delete'


# для создания категории нужен КАТАЛОГ КАСТОМНЫЙ
# для создания каталога нужен кастомный WEBSITE
# для создания вебсайта нужен кастомный USER
# бзер должен быть уникальный для каждого магазина
def create_custom_catalog(company_url):
    print(company_url)
    if not Website.objects.filter(name = company_url):

        user1 = User.objects.create_user(
            username = 'parsexml{}'.format(random.randint(1,1000)),
            email = 'parsexml@gmail.com',
            password = 'zaza1234'
        )
        user1.is_active = True
        user1.is_superuser = True
        user1.is_staff = True
        user1.save()

        website1 = Website.objects.get_or_create(
            user = user1,
            domain = company_url,
            subdomain = company_url,
            name = company_url
        )
        
        # create custom catalog
        catalog1 = Catalog.objects.get_or_create(website = website1[0])
    else:
        website1 = Website.objects.filter(name = company_url)
        catalog1 = Catalog.objects.filter(website = website1)

    print(website1)
    print(catalog1)
    print('Data was created!!!')
    return website1[0], catalog1[0]    

def get_categories(xmldoc, decission_to_db, company_url):
    categories = xmldoc.getElementsByTagName('category')
    for nmb, cat in enumerate(categories):
        ids = cat.attributes['id'].value
        title = cat.firstChild.nodeValue

        # получаем нашу категорию
        cat1 = Category.objects.get_or_create(
            inner_id = ids,
            name = title,
            catalog = get_object_or_404(Catalog, website__name = company_url)
        )
        cat1 = cat1[0]

        # если действие "created" - то мы меняем Удален на False, и наоборот
        if decission_to_db == 'create':
            cat1.deleted = False
            cat1.save()
        else:
            cat1.deleted = True
            cat1.save()
            
            # удаляем все товары относящиеся к данной категории
            Item.objects.filter(category = cat1).delete()
        print('{} - some action with category - {}: {}'.format(nmb, cat1, decission_to_db))
    print('Finish with categories.')
    return ''
    
            



def get_offers(xmldoc, decission_to_db, company_url):
    
    offers = xmldoc.getElementsByTagName('offer')
    for nmb, offer in enumerate(offers):
        try:
            ids = offer.attributes['id'].value
            categoryId = offer.getElementsByTagName('categoryId')[0].firstChild.nodeValue
            currencyId = offer.getElementsByTagName('currencyId')[0].firstChild.nodeValue
            description = offer.getElementsByTagName('description')[0].firstChild.nodeValue
            modified_time = offer.getElementsByTagName('modified_time')[0].firstChild.nodeValue
            name = offer.getElementsByTagName('name')[0].firstChild.nodeValue

            # картинок несколько
            pictures = offer.getElementsByTagName('picture')
            all_pict = []
            [all_pict.append(pict.firstChild.nodeValue) for pict in pictures]

            price = offer.getElementsByTagName('price')[0].firstChild.nodeValue
            url = offer.getElementsByTagName('url')[0].firstChild.nodeValue
            vendor = offer.getElementsByTagName('vendor')[0].firstChild.nodeValue
            vendorCode = offer.getElementsByTagName('vendorCode')[0].firstChild.nodeValue

            # save data to db
            # create vendor if not exists
            vendor1 = Vendor.objects.get_or_create(
                name = vendor,
            )

            item1 = Item(
                inner_id = ids,
                site = get_object_or_404(Website, name = company_url),
                category = Category.objects.get(inner_id = categoryId, catalog = get_object_or_404(Catalog, website__name = company_url)),
                currency = currencyId,
                description = description,
                name = name, 
                price = price,
                url = url,
                vendor = vendor1[0]
            )

            if len(all_pict) > 1:
                item1.image_url = all_pict[0]
                item1.save()

            print('{} - Item with name "{}" was added.'.format(nmb, name))
        except:
            print('Was some problem!!!')
    print('Finish with offers.')
    return '' 



def get_url_company(xmldoc):
    if xmldoc.getElementsByTagName('url'):
        data = xmldoc.getElementsByTagName('url')[0].firstChild.nodeValue
        corr_d = data.replace('https://', '').replace('http://','')
    else:
        corr_d = 'no-data'
    return corr_d


base_folder = os.path.join(settings.STATIC_ROOT, 'import_task_data/')

# get list of files
list_of_files = get_list_of_files(base_folder)

for file_name in list_of_files:
    path_full = os.path.join(base_folder, file_name)
    file_size = os.stat(path_full).st_size

    # print('*'*150)
    # print(file_name)
    # print('*'*150)

    # check if size if not empty
    # окрываем файлы весом меньше 500 метров
    if file_size != 0 and file_size < 100047104:
        xmldoc = minidom.parse(path_full)

        # create custom catalog if not exists - это для категорий
        company_url = get_url_company(xmldoc)
        web, cat = create_custom_catalog(company_url)

        # get date of file
        xml_date = xmldoc.getElementsByTagName('yml_catalog')[0].attributes['date'].value
        decission_to_db = add_to_db_or_not(xml_date)

        print('Date from xml file is - {}'.format(xml_date))

        # find categories
        ctg = get_categories(xmldoc, decission_to_db, company_url)

        # # find offers. удалем товары мы в функции выше, тут только проверка на добавление
        if decission_to_db == 'create':
            ofr = get_offers(xmldoc, decission_to_db, company_url)

print('All files were added!!!')


